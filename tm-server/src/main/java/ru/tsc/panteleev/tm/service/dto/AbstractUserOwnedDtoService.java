package ru.tsc.panteleev.tm.service.dto;

import org.springframework.stereotype.Service;
import ru.tsc.panteleev.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.tsc.panteleev.tm.api.service.dto.IUserOwnedDtoService;
import ru.tsc.panteleev.tm.dto.model.AbstractUserOwnedModelDto;

@Service
public abstract class AbstractUserOwnedDtoService<M extends AbstractUserOwnedModelDto, R extends IUserOwnedDtoRepository<M>>
        extends AbstractDtoService<M, R> implements IUserOwnedDtoService<M> {

}
