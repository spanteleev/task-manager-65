package ru.tsc.panteleev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.model.Project;

import java.util.*;

@Repository
public class ProjectRepository {

    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        for (int i = 1; i < 11; i++) {
            add(new Project("Project " + i, UUID.randomUUID().toString()));
        }
    }

    public void create() {
        add(new Project(("Project " + System.currentTimeMillis() / 1000), UUID.randomUUID().toString()));
    }

    public void add(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    public void save(@NotNull final Project project) {
        projects.replace(project.getId(), project);
    }

    @Nullable
    public Collection<Project> findAll() {
        return projects.values();
    }

    @Nullable
    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public boolean existsById(@NotNull final String id) {
        return projects.containsKey(id);
    }

    public long count() {
        return projects.size();
    }

    public void delete(@NotNull final Project project) {
        projects.remove(project.getId());
    }

    public void deleteById(@NotNull final String id) {
        projects.remove(id);
    }

    public void deleteAll(@NotNull final List<Project> projectList) {
        projectList.forEach(this::delete);
    }

    public void clear() {
        projects.clear();
    }

}
