package ru.tsc.panteleev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.model.Task;

import java.util.*;

@Repository
public class TaskRepository {

    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        for (int i = 1; i < 11; i++) {
            add(new Task("Task " + i, UUID.randomUUID().toString()));
        }
    }

    public void create() {
        add(new Task(("Task " + System.currentTimeMillis() / 1000), UUID.randomUUID().toString()));
    }

    public void add(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    public void save(@NotNull final Task task) {
        tasks.replace(task.getId(), task);
    }

    @Nullable
    public Collection<Task> findAll() {
        return tasks.values();
    }

    @Nullable
    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public boolean existsById(@NotNull final String id) {
        return tasks.containsKey(id);
    }

    public long count() {
        return tasks.size();
    }

    public void delete(@NotNull final Task task) {
        tasks.remove(task.getId());
    }

    public void deleteById(@NotNull final String id) {
        tasks.remove(id);
    }

    public void deleteAll(@NotNull final List<Task> taskList) {
        taskList.forEach(this::delete);
    }

    public void clear() {
        tasks.clear();
    }


}
