<%@ page contentType="text/html;charset=UTF-8" language="java" %>
</body>
    <head>
        <title>Task Manager</title>
    </head>
    <style>
        b {
            font-size: 2em;
            padding-left: 10px;
        }
        h1 {
            font-size: 1.8em;
        }
        a {
            color: blue;
            padding: 10px;
        }
        th {
            font-size: 1.4em;
            padding: 10px;
        }
        button {
            font-size: 1.4em;
            padding: 10px;
        }
        select {
            width: 200px;
        }
        input[type="text"] {
            width: 200px;
        }
        input[type="date"] {
            width: 200px;
        }
    </style>

<body>
    <table width="100%" height="100%" border="1" style="padding: 10px;">
        <tr>
            <td height="35" width="40%" nowrap="nowrap" align="left" font-size="80">
                <b>Task Manager</b>
            </td>

            <td width="60%" align="right">
                <a href="/projects">Projects</a>
                <a href="/tasks">Tasks</a>
            </td>
        </tr>
        <tr>
            <td colspan="2" height="100%" valign="top" style="padding: 10px;">
